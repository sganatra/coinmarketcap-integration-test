#Author: Sachin Ganatra
Feature: 04 Retrieving specific cryptocurrency and converting it to bolivian

  Scenario Outline: Retrieving and converting cryptocurrency
    Given I have an ID of <cryptocurrency>
    When I convert the same to <price> Bolivian for amount 50
    Then I confirm it has been converted

    Examples: 
      | cryptocurrency | price |
      | "BTC"          | "BOB" |
      | "USDT"         | "BOB" |
      | "ETH"          | "BOB" |
