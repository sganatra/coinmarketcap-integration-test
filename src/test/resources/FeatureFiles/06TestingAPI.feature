#Author: Sachin Ganatra
Feature: 06 Retrieving specific cryptocurrency and verifying tag has mineable

  Scenario Outline: Retrieving and verifying cryptocurrency tags detail
    Given I have an ID <id> of cryptocurrency
    When I retrieve tag information of this cryptocurrency
    Then I verify tag has mineable true

    Examples: 
      | id   |
      | "1"  |
      | "2"  |
      | "3"  |
      | "4"  |
      | "5"  |
      | "6"  |
      | "7"  |
      | "8"  |
      | "9"  |
      | "10" |
