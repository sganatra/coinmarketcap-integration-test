#Author: Sachin Ganatra
Feature: 03 Filtering table and verifying data accordingly

  Scenario: Open coinmarketcap website
    Given I have URL "https://coinmarketcap.com/"
    When I open the link
    Then I confirm page title has "Cryptocurrency Market Capitalizations"
		When I click on cryptocurrency dropdown and first full list from menu
		
  Scenario Outline: Filter cryptocurrecny table based on available opton and verify data
    Then get the size of the table
    When user changes <filteroption> to <value>
    Then verify number of records and filter options matches filter value

    Examples: 
      | filteroption | value            |
      | "marketCap"  | "oneBillionPlus" |
      | "price"      | "hundredPlus"    |
      | "volume"     | "tenMillionPlus" |

  @lastStep
  Scenario: Closing driver
