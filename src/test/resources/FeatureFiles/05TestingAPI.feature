#Author: Sachin Ganatra
Feature: 05 Retrieving specific cryptocurrency info and verifying

  Scenario: Retrieving and verifying cryptocurrency details
    Given I have an ID 1027 of cryptocurrency
    When I retrieve the information of this cryptocurrency
    Then I verify all the details
      | metadata      | value                                                        |
      | logo          | https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png |
      | technical_doc | https://github.com/ethereum/wiki/wiki/White-Paper            |
      | symbol        | ETH                                                          |
      | date_added    | 2015-08-07T00:00:00.000Z                                     |
      | platform      | null                                                         |
      | tags          | mineable                                                     |
