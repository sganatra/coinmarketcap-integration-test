#Author: Sachin Ganatra
Feature: 01 Opening CoinMarketCap and View All

  Scenario: Click on ViewAll and verify results
    Given I have URL "https://coinmarketcap.com/"
    When I open the link
    Then I confirm page title has "Cryptocurrency Market Capitalizations"
    When I click on ViewAll
    Then I verify atleast 100 results are displayed

  @lastStep
  Scenario: Closing driver
