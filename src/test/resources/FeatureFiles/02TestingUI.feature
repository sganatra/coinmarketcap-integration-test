#Author: Sachin Ganatra
Feature: 02 Adding cryptocurrency and verifying watchlist

  Scenario: Add cryptocurrency to watchlist and verify the same
    Given I have URL "https://coinmarketcap.com/"
    When I open the link
    Then I confirm page title has "Cryptocurrency Market Capitalizations"
    Given I have a list of cryptocurrency
    When I add few cryptocurrency between record five and ten to watchlist
    Then I verify all added to the list

  @lastStep
  Scenario: Closing driver
