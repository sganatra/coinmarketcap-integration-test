package com.coinmarketcap.apiIntegrationTest;

import com.coinmarketcap.apiDefinition.ConfigAPI;
import com.jayway.restassured.response.Response;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BE01Testing extends ConfigAPI {
	int id;
	Response response, response1;

	@Given("^I have an ID of \"(.*?)\"$")
	public void i_have_an_ID_of(String cryptoCurrency) throws Throwable {
		response = cryptocurrencyAPI.getCryptocurrencyMap();

		for (int i = 0; i < 5000; i++) {
			if (response.jsonPath().getString("data[" + i + "].symbol").contains(cryptoCurrency)) {
				id = response.jsonPath().getInt("data[" + i + "].id");
				System.out.println("The cryptoCurrency " + cryptoCurrency + " has ID = " + id);
				break;
			}

		}
	}

	@When("^I convert the same to \"(.*?)\" Bolivian for amount (\\d+)$")
	public void i_convert_the_same_to_Bolivian_for_amount(String conversionSymbol, int amount) throws Throwable {
		response1 = priceConversionAPI.getPriceConversion(id, amount, conversionSymbol);
	}

	@Then("^I confirm it has been converted$")
	public void i_confirm_it_has_been_converted() throws Throwable {
		System.out.println("Error Code: " + response1.jsonPath().getInt("status.error_code"));
		assertThat(response1.jsonPath().getInt("status.error_code"), is(equalTo(0)));
	}

}
