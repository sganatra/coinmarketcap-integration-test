package com.coinmarketcap.apiIntegrationTest;

import com.coinmarketcap.apiDefinition.ConfigAPI;
import com.jayway.restassured.response.Response;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BE03Testing extends ConfigAPI {
	Response response = null;
	int id;
	String tagString = null;

	@Given("^I have an ID \"(.*?)\" of cryptocurrency$")
	public void i_have_an_ID_of_cryptocurrency(int id) throws Throwable {
		System.out.println("The ID of cryptoCurrency is " + id);
		this.id = id;
	}

	@When("^I retrieve tag information of this cryptocurrency$")
	public void i_retrieve_tag_information_of_this_cryptocurrency() throws Throwable {
		response = cryptocurrencyAPI.getCryptocurrencyInfo(id);
		tagString = response.jsonPath().getString("data." + id + ".tags[0]");
		System.out.println("The " + id + " has tags = " + tagString);
	}

	@Then("^I verify tag has mineable true$")
	public void i_verify_tag_has_mineable_true() throws Throwable {
		if (tagString.contains("mineable")) {
			System.out.println("Cryptocurrency containing mineable tags: "
					+ response.jsonPath().getString("data." + id + ".name"));
		}
	}

}
