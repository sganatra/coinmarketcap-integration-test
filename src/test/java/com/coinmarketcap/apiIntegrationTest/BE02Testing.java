package com.coinmarketcap.apiIntegrationTest;

import java.util.List;

import com.coinmarketcap.apiDefinition.ConfigAPI;
import com.jayway.restassured.response.Response;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BE02Testing extends ConfigAPI {
	Response response;
	int id;

	@Given("^I have an ID (\\d+) of cryptocurrency$")
	public void i_have_an_ID_of_cryptocurrency(int id) throws Throwable {
		System.out.println("The ID of cryptoCurrency is " + id);
		this.id = id;
	}

	@When("^I retrieve the information of this cryptocurrency$")
	public void i_retrieve_the_information_of_this_cryptocurrency() throws Throwable {
		response = cryptocurrencyAPI.getCryptocurrencyInfo(id);
	}

	@Then("^I verify all the details$")
	public void i_verify_all_the_details(DataTable dataTable) throws Throwable {
		List<List<String>> data = dataTable.raw();

		assertThat(data.get(1).get(1).toString(), containsString(response.jsonPath().getString("data.1027.logo")));
		assertThat(data.get(2).get(1).toString(),
				containsString(response.jsonPath().getString("data.1027.urls.technical_doc[0]")));
		assertThat(data.get(3).get(1).toString(), containsString(response.jsonPath().getString("data.1027.symbol")));
		assertThat(data.get(4).get(1).toString(),
				containsString(response.jsonPath().getString("data.1027.date_added")));
		assertThat(response.jsonPath().getString("data.1027.platform"), is(nullValue()));
		assertThat(data.get(6).get(1).toString(), containsString(response.jsonPath().getString("data.1027.tags[0]")));
	}
}
