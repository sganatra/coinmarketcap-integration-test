package com.coinmarketcap.apiDefinition;

import static com.jayway.restassured.RestAssured.*;

import com.jayway.restassured.response.Response;

public class PriceConversionAPI extends ConfigAPI {

	public PriceConversionAPI() {
		ConfigAPI.main(null);
	}
	
	public Response getPriceConversion(int id, int amount, String conversionString) {
		return given().header("X-CMC_PRO_API_KEY", "c23edf6b-40c0-4913-a048-faec82582c6e").parameter("id", id)
				.parameter("amount", amount).parameter("convert", conversionString).expect().statusCode(200).when()
				.get("tools/price-conversion");

	}

}
