package com.coinmarketcap.apiDefinition;

import static com.jayway.restassured.RestAssured.*;

import com.jayway.restassured.response.Response;

public class CryptocurrencyAPI extends ConfigAPI {

	public CryptocurrencyAPI() {
		ConfigAPI.main(null);
	}

	public Response getCryptocurrencyMap() {
		return given().header("X-CMC_PRO_API_KEY", "c23edf6b-40c0-4913-a048-faec82582c6e").expect().statusCode(200)
				.when().get("cryptocurrency/map");

	}
	
	public Response getCryptocurrencyInfo(int id) {
		return given().header("X-CMC_PRO_API_KEY", "c23edf6b-40c0-4913-a048-faec82582c6e").parameter("id", id).expect().statusCode(200)
				.when().get("cryptocurrency/info");

	}
}
