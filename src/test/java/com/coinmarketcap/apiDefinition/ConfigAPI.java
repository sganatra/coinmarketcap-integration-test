package com.coinmarketcap.apiDefinition;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.authentication.PreemptiveBasicAuthScheme;

public class ConfigAPI {
	public static String apiKey = "c23edf6b-40c0-4913-a048-faec82582c6e";
	public ObjectNode jsonObjectMapper;
	
	// Class pointers
	public static CryptocurrencyAPI cryptocurrencyAPI = new CryptocurrencyAPI();
	public static PriceConversionAPI priceConversionAPI = new PriceConversionAPI();
	
	public static void main(String[] args) {
		RestAssured.baseURI = "https://pro-api.coinmarketcap.com/v1/";
	}

}
