package com.coinmarketcap.uiIntegrationTest;

import com.coinmarketcap.pageObjects.Config;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import org.openqa.selenium.WebDriver;

import static org.hamcrest.Matchers.*;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FE01TestingUI extends Config {
	
	static String url;

	@Given("^I have URL \"(.*?)\"$")
	public void i_have_URL(String url) throws Throwable {
		this.url = url;
	}

	@When("^I open the link$")
	public void i_open_the_link() throws Throwable {
		driver.get(url);
	}

	@Then("^I confirm page title has \"(.*?)\"$")
	public void i_confirm_page_title_has(String title) throws Throwable {
		assertThat(driver.getTitle(), containsString(title));
	}

	@When("^I click on ViewAll$")
	public void i_click_on_ViewAll() throws Throwable {
		homePagePO.viewAllButton.click();
		Thread.sleep(10000);
	}

	@Then("^I verify atleast (\\d+) results are displayed$")
	public void i_verify_atleast_results_are_displayed(int numberOfRows) throws Throwable {
		Displayed(homePagePO.backToTop100Button);
		assertThat(homePagePO.totalRowsReturnedViewAll.size(), greaterThanOrEqualTo(numberOfRows));
	}

	@After("@lastStep")
	public void driverQuit() throws Throwable {
		driver.quit();
	}

}
