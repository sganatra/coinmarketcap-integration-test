package com.coinmarketcap.uiIntegrationTest;

import com.coinmarketcap.pageObjects.Config;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.text.NumberFormat;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FE03TestingUI extends Config {
	
	String value = null;
	String value1 = null;
	
	@When("^I click on cryptocurrency dropdown and first full list from menu$")
	public void i_click_on_cryptocurrency_dropdown_and_first_full_list_from_menu() throws Throwable {
		homePagePO.cryptoCurrencyDropdown.click();
		Displayed(homePagePO.cryptoCurrencyDropdownFullList.get(0));
		homePagePO.cryptoCurrencyDropdownFullList.get(0).click();
		Thread.sleep(5000);
	}

	@Then("^get the size of the table$")
	public void get_the_size_of_the_table() throws Throwable {
		Displayed(homePagePO.filterListValue.get(0));
		System.out.println("The size of the table is: " + homePagePO.mainTable.size());
	}

	@When("^user changes \"(.*?)\" to \"(.*?)\"$")
	public void user_changes_to(String filterList, String value) throws Throwable {
		Displayed(homePagePO.filterListValue.get(0));
		this.value = value;
		if (filterList.contains("marketCap") && value.contains("oneBillionPlus")) {
			homePagePO.filterListValue.get(0).click();
			Displayed(homePagePO.marketCapOneBillionPlus);
			homePagePO.marketCapOneBillionPlus.click();
			Thread.sleep(20000);
		} else if (filterList.contains("price") && value.contains("hundredPlus")) {
			homePagePO.filterListValue.get(1).click();
			Displayed(homePagePO.priceOptionHundredPlus);
			homePagePO.priceOptionHundredPlus.click();
		} else if (filterList.contains("volume") && value.contains("tenMillionPlus")) {
			homePagePO.filterListValue.get(2).click();
			Displayed(homePagePO.volumeOptionTenMillionPlus);
			homePagePO.volumeOptionTenMillionPlus.click();
		}
		Thread.sleep(10000);
	}

	@Then("^verify number of records and filter options matches filter value$")
	public void verify_number_of_records_and_filter_options_matches_filter_value() throws Throwable {
		System.out.println("The size of the NEW table is: " + homePagePO.mainTable.size());
		if (value.contains("oneBillionPlus")) {
			for (int i = 2; i < homePagePO.mainTable.size(); i++) {
				System.out.println(homePagePO.marketCapCurrencyName.get(i).getText() + " = "
						+ homePagePO.marketCapColumnValue.get(i).getText());
				assertThat(currencyToInteger(homePagePO.marketCapColumnValue.get(i).getText()),
						is(greaterThan(Double.parseDouble("1000000000"))));
			}
		} else if (value.contains("hundredPlus")) {
			for (int i = 2; i < homePagePO.mainTable.size(); i++) {
				System.out.println(homePagePO.marketCapCurrencyName.get(i).getText() + " = "
						+ homePagePO.priceValueColumn.get(i).getText());
				assertThat(currencyToInteger(homePagePO.priceValueColumn.get(i).getText()),
						is(greaterThan(Double.parseDouble("100"))));
			}
		} else if (value.contains("tenMillionPlus")) {
			for (int i = 2; i < homePagePO.mainTable.size(); i++) {
				System.out.println(homePagePO.marketCapCurrencyName.get(i).getText() + " = "
						+ homePagePO.volumeValueColumn.get(i).getText());
				assertThat(currencyToInteger(homePagePO.volumeValueColumn.get(i).getText()),
						is(greaterThan(Double.parseDouble("10000000"))));
			}
		}

	}

	public double currencyToInteger(String currency) {
		currency = currency.replace(NumberFormat.getCurrencyInstance().getCurrency().getSymbol(), "");
		currency = currency.replace("$", "");
		currency = currency.replace(",", "");
		System.out.println(currency);
		System.out.println(currency.replace(",", ""));
		System.out.println(currency);
		return Double.parseDouble(currency);
	}
}
