package com.coinmarketcap.uiIntegrationTest;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import com.coinmarketcap.pageObjects.Config;
import com.google.inject.internal.util.Lists;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FE02TestingUI extends Config {
	
	public FE02TestingUI(WebDriver driver){
		Config.main(null);
		this.driver = driver;
	}
	
	static int randomInt;
	static String[] cryptoCurrency = new String[2];

	
	@Given("^I have a list of cryptocurrency$")
	public void i_have_a_list_of_cryptocurrency() throws Throwable {
		System.out.println("Size of list displayed = " + homePagePO.totalRowsReturnedViewAll.size());
	}

	@When("^I add few cryptocurrency between record five and ten to watchlist$")
	public void i_add_few_cryptocurrency_between_record_five_and_ten_to_watchlist() throws Throwable {
		scrollToElement(homePagePO.ellipsisButton.get(3));
		for (int i = 0; i < 2; i++) {
			randomInt = ThreadLocalRandom.current().nextInt(5, 10);
			System.out.println("Random number generated is : " + randomInt);
			homePagePO.ellipsisButton.get(randomInt).click();
			Thread.sleep(2000);
			homePagePO.addToWatchlistButton.click();
			cryptoCurrency[i] = homePagePO.cryptocurrencyName.get(randomInt-1).getText();
		}

	}

	@Then("^I verify all added to the list$")
	public void i_verify_all_has_been_added_to_the_list() throws Throwable {		
		//Actions action=new Actions(driver);
		action.keyDown(Keys.CONTROL).build().perform();
		homePagePO.mainMenuWatchlistLink.click();
		action.keyUp(Keys.CONTROL).build().perform();
		windowsHandle();
		Displayed(homePagePO.cryptocurrencyName.get(0));
		
		for (int i = 0; i < 2; i++) {
			System.out.println("Cryptocurrency CURRENT Name: " + homePagePO.cryptocurrencyName.get(i).getText());
			List<String> collection = Lists.newArrayList(cryptoCurrency[0].toString(), cryptoCurrency[1].toString());
			assertThat(collection, hasItem(homePagePO.cryptocurrencyName.get(i).getText()));
			
		}

	}

}
