package com.coinmarketcap.pageObjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePagePO extends Config {

	public HomePagePO(WebDriver driver) {

	}

	// Home Page - View All Button
	@FindBy(linkText = "View All")
	public WebElement viewAllButton;

	// Home Page - Number of rows returned
	@FindBy(css = ".cmc-table-row")
	public List<WebElement> totalRowsReturnedViewAll;

	// Home Page - Back to Top 100
	@FindBy(partialLinkText = "Back to Top 100")
	public WebElement backToTop100Button;

	// Home Page - Ellipsis
	@FindBy(css = ".svg-inline--fa.fa-ellipsis-h.fa-w-16.cmc-icon")
	public List<WebElement> ellipsisButton;

	// Home Page - Add to Watchlist
	@FindBy(xpath = "//span[.='Add to Watchlist']")
	public WebElement addToWatchlistButton;

	// Home Page - Cryptocurrency name
	@FindBy(css = ".cmc-table__column-name.sc-1kxikfi-0.eTVhdN")
	public List<WebElement> cryptocurrencyName;

	// Home Page - mainMenuWatchlistLink
	@FindBy(xpath = "//a[@href='/watchlist/']")
	public WebElement mainMenuWatchlistLink;

	// Home Page - Cryptocurrency Dropdown
	@FindBy(xpath = "//span[.='Cryptocurrencies']")
	public WebElement cryptoCurrencyDropdown;

	// Home Page - cryptoCurrencyDropdownFullList
//	@FindBy(xpath = "//a[@href='/coins/views/all/']")
	@FindBy(partialLinkText = "Full List")
	public List<WebElement> cryptoCurrencyDropdownFullList;

	// Home Page - Table
	@FindBy(css = ".cmc-table-row")
	public List<WebElement> mainTable;
	/*
	 * // Home Page - FilterList
	 * 
	 * @FindBy(css = ".css-vj8t7z.cmc-select__control") public List<WebElement>
	 * filterListBox;
	 */
	// Home Page - FilterListValue
	@FindBy(css = ".css-xp4uvy.cmc-select__single-value")
	public List<WebElement> filterListValue;

	// Home Page - Market Cap ALL Option
	@FindBy(id = "react-select-market-option-0")
	public WebElement marketCapAll;

	// Home Page - Market Cap OneBillionPlus Option
	@FindBy(id = "react-select-market-option-1")
	public WebElement marketCapOneBillionPlus;

	// Home Page - Price Option ALL Option
	@FindBy(id = "react-select-price-option-0")
	public WebElement priceOptionAll;

	// Home Page - Price Option HundredPlus Option
	@FindBy(id = "react-select-price-option-1")
	public WebElement priceOptionHundredPlus;

	// Home Page - Volume Option ALL Option
	@FindBy(id = "react-select-volume-option-0")
	public WebElement volumeOptionAll;

	// Home Page - Volume Option HundredPlus Option
	@FindBy(id = "react-select-volume-option-1")
	public WebElement volumeOptionTenMillionPlus;
	
	//Home Page - Market Cap column value
	@FindBy (css = ".cmc-table__cell.cmc-table__cell--sortable.cmc-table__cell--right.cmc-table__cell--sort-by__market-cap")
	public List<WebElement> marketCapColumnValue;
	
	//Home Page - Cryptocurrency name column value
	@FindBy (css = ".cmc-table__column-name.sc-1kxikfi-0.eTVhdN")
	public List<WebElement> marketCapCurrencyName;
	
	//Home Page Price Value column
	@FindBy(css = ".cmc-table__cell.cmc-table__cell--sortable.cmc-table__cell--right.cmc-table__cell--sort-by__price")
	public List<WebElement> priceValueColumn;
	
	//Home Page Volume Value column
	@FindBy(css = ".cmc-table__cell.cmc-table__cell--sortable.cmc-table__cell--right.cmc-table__cell--sort-by__volume-24-h")
	public List<WebElement> volumeValueColumn;
}
