package com.coinmarketcap.pageObjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.googlecode.totallylazy.Arrays;

public class Config {
	protected static WebDriver driver = getRemoteWebDriver();
	public static HomePagePO homePagePO = PageFactory.initElements(driver, HomePagePO.class);
	protected Actions action = new Actions(driver);

	public static void main(String[] args) {

	}

	public static WebDriver getRemoteWebDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		ChromeOptions chromeOptions = new ChromeOptions();
		// chromeOptions.addArguments("--headless");
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
		desiredCapabilities.setCapability("chrome.switches", Arrays.list("--incognito"));

		desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

		@SuppressWarnings("deprecation")
		WebDriver driver = new ChromeDriver(desiredCapabilities);
		driver.manage().window().maximize();
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("document.body.style.zoom='0.75'");
		return driver;

	}

	public void scrollToElement(WebElement element) throws Throwable {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		Displayed(element);
	}

	public String parentHandle;

	public void windowsHandle() {
		parentHandle = driver.getWindowHandle();
		System.out.println(parentHandle);

		for (String winHandle : driver.getWindowHandles()) {
			System.out.println(winHandle);
			driver.switchTo().window(winHandle);
		}

	}

	public void Displayed(WebElement element) throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfAllElements(element));
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

}
