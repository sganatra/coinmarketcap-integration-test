package com.coinmarketcap.pageObjects;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/FeatureFiles/", monochrome = true, plugin = { "pretty",
		"html:target/cucumber-html-reports",
		"json:target/cucumber.json" }, glue = { "com.coinmarketcap.uiIntegrationTest", "com.coinmarketcap.apiIntegrationTest" })

public class RunCukesTest {

}
