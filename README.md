To Run the Test:
1. Download chromeDriver.exe FROM coinmarketcap-integration-test\src\test\resources\chromedriver.exe TO C:\
2. Once project is pulled, you can run feature files accordingly.
3. Currently test 03TestingUI.feature is failing due to a bug on CoinMarketCap. Bug is when the user tries to filter market cap with One Billion Plus, results still gives amount for crypto currency that has value less than 1 Billion plus. 